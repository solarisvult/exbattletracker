package main.Players.Weapons.Mortal.melee.light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.light.LightMeleeWeapon;

public class TigerClaws extends LightMeleeWeapon{
	public TigerClaws(){
		super();
		this.setName("Tiger Claws");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.BRAWL);
		wt.add(WeaponTags.WORN);
		this.setTags(wt);
	}
}
