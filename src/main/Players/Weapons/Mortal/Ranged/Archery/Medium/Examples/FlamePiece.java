package main.Players.Weapons.Mortal.Ranged.Archery.Medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Archery.Medium.MediumArcheryWeapon;

public class FlamePiece extends MediumArcheryWeapon{
	public FlamePiece(){
		super();
		this.setName("Flame Piece");
		this.putMaxRange(WeaponTags.ARCHERY, RangeBands.SHORT);
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.ARCHERY);
		wt.add(WeaponTags.MOUNTED);
		wt.add(WeaponTags.ONEHANDED);
		wt.add(WeaponTags.SLOW);
		wt.add(WeaponTags.FLAME);
		this.setTags(wt);
	}
}
