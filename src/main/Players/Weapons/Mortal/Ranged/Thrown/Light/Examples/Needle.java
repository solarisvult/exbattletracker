package main.Players.Weapons.Mortal.Ranged.Thrown.Light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Thrown.Light.LightThrownWeapon;

public class Needle extends LightThrownWeapon{
	public Needle(){
		super();
		this.setName("Needle");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.THROWN);
		this.putMaxRange(WeaponTags.THROWN, RangeBands.SHORT);
		wt.add(WeaponTags.CONCEALABLE);
		wt.add(WeaponTags.MOUNTED);
		wt.add(WeaponTags.POISONABLE);
		this.setTags(wt);
		//TODO ADD BLOWDART
		//TODO ADD POISONABLE FUNCTIONALITY
		
	}
}
