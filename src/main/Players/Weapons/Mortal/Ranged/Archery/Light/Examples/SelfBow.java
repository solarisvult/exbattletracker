package main.Players.Weapons.Mortal.Ranged.Archery.Light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Archery.Light.LightArcheryWeapon;

public class SelfBow extends LightArcheryWeapon{
	public SelfBow(){
		super();
		this.setName("Self Bow");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.ARCHERY);
		wt.add(WeaponTags.MOUNTED);
		this.setTags(wt);
	}
}
