package main.Players.Weapons;

import java.util.ArrayList;
import java.util.TreeMap;

import main.Damage;
import main.Players.Abilities;

public class Weapon {
	private String name;
	//TODO Change accuracy to include multiple range bands
	private TreeMap<RangeBands, Integer> accuracy;
	private int damage;
	private int defense;
	private int overwhelming;
	private TreeMap<WeaponTags, RangeBands> maxRange;
	private ArrayList<WeaponTags> tags;
	
	public Weapon(  int damage, 
					int defense, 
					int overwhelming){
		this.damage = damage;
		this.defense = defense;
		this.overwhelming = overwhelming;
		this.maxRange = new TreeMap<WeaponTags, RangeBands>();
	}
	
	public int GetAccuracyFromRangeBand(RangeBands Range){
		int accuracy = getAccuracy().get(Range);
		return accuracy;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TreeMap<RangeBands, Integer> getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(TreeMap<RangeBands, Integer> accuracy) {
		this.accuracy = accuracy;
	}
	public int getDamage() {
		return damage;
	}
	public void setDamage(int damage) {
		this.damage = damage;
	}
	public int getDefense() {
		return defense;
	}
	public void setDefense(int defense) {
		this.defense = defense;
	}
	public int getOverwhelming() {
		return overwhelming;
	}
	public void setOverwhelming(int overwhelming) {
		this.overwhelming = overwhelming;
	}
	public ArrayList<WeaponTags> getTags() {
		return tags;
	}
	public void setTags(ArrayList<WeaponTags> tags) {
		this.tags = tags;
	}

	public TreeMap<WeaponTags, RangeBands> getMaxRange() {
		return maxRange;
	}

	public void setMaxRange(TreeMap<WeaponTags, RangeBands> maxRange) {
		this.maxRange = maxRange;
	}
	
	public void putMaxRange(WeaponTags wt, RangeBands mr) {
		this.maxRange.put(wt, mr);
	}
	
	public RangeBands getMaxRange(WeaponTags wt){
		return this.maxRange.get(wt);
	}
}
