package main.Players.Weapons.Mortal.melee.light;

import java.util.ArrayList;
import java.util.TreeMap;

import main.Damage;
import main.Players.Abilities;
import main.Players.Weapons.RangeBands;
import main.Players.Weapons.Weapon;
import main.Players.Weapons.WeaponTags;

public class LightMeleeWeapon extends Weapon{

	public LightMeleeWeapon() {
		super(7, 0, 1);
		this.putMaxRange(WeaponTags.MELEE, RangeBands.CLOSE);
		TreeMap<RangeBands, Integer> acc = new TreeMap<RangeBands, Integer>();
		acc.put(RangeBands.CLOSE, 4);
		acc.put(RangeBands.SHORT, 0);
		acc.put(RangeBands.MEDIUM, 0);
		acc.put(RangeBands.LONG, 0);
		acc.put(RangeBands.EXTREME, 0);
		this.setAccuracy(acc);
	}
}
