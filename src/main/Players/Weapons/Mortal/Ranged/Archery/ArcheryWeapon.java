package main.Players.Weapons.Mortal.Ranged.Archery;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.Weapon;
import main.Players.Weapons.WeaponTags;

public class ArcheryWeapon extends Weapon{

	public ArcheryWeapon(int damage, int defense, int overwhelming) {
		super(damage, defense, overwhelming);
		this.putMaxRange(WeaponTags.ARCHERY, RangeBands.LONG);
	}

}
