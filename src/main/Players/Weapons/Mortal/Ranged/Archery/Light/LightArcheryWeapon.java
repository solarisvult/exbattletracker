package main.Players.Weapons.Mortal.Ranged.Archery.Light;

import main.Players.Weapons.Mortal.Ranged.Archery.ArcheryWeapon;

public class LightArcheryWeapon extends ArcheryWeapon{

	public LightArcheryWeapon() {
		super(7, 0, 1);
	}

}
