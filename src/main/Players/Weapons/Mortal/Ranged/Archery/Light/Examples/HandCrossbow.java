package main.Players.Weapons.Mortal.Ranged.Archery.Light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Archery.Light.LightArcheryWeapon;

public class HandCrossbow extends LightArcheryWeapon{
	public HandCrossbow(){
		super();
		this.setName("Hand Crossbow");
		this.putMaxRange(WeaponTags.ARCHERY, RangeBands.MEDIUM);
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.ARCHERY);
		wt.add(WeaponTags.CROSSBOW);
		wt.add(WeaponTags.MOUNTED);
		wt.add(WeaponTags.ONEHANDED);
		wt.add(WeaponTags.PIERCING);
		wt.add(WeaponTags.SLOW);
		wt.add(WeaponTags.CONCEALABLE);
		this.setTags(wt);
	}
}
