package main.Players.Weapons.Mortal.melee.medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.medium.MediumMeleeWeapon;

public class ShortSpear extends MediumMeleeWeapon{
	public ShortSpear(){
		super();
		this.setName("ShortSpear");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.MELEE);
		wt.add(WeaponTags.THROWN);
		this.putMaxRange(WeaponTags.THROWN, RangeBands.SHORT);
		wt.add(WeaponTags.PIERCING);
		this.setTags(wt);
	}
}