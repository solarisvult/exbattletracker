package main.Players.Weapons.Mortal.Ranged.Archery.Medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Archery.Medium.MediumArcheryWeapon;

public class CompositeBow extends MediumArcheryWeapon{
	public CompositeBow(){
		super();
		this.setName("Composite Bow");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.ARCHERY);
		wt.add(WeaponTags.MOUNTED);
		this.setTags(wt);
	}
}
