package main.Players.Weapons.Mortal.melee.light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.light.LightMeleeWeapon;

public class Unarmed extends LightMeleeWeapon{
	public Unarmed(){
		this.setName("Unarmed");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.BASHING);
		wt.add(WeaponTags.BRAWL);
		wt.add(WeaponTags.GRAPPLING);
		wt.add(WeaponTags.NATURAL);
		this.setTags(wt);
	}
}
