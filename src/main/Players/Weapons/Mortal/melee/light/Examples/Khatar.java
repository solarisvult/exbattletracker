package main.Players.Weapons.Mortal.melee.light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.light.LightMeleeWeapon;

public class Khatar extends LightMeleeWeapon{

	public Khatar() {
		super();
		this.setName("Khatar");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.BRAWL);
		wt.add(WeaponTags.PIERCING);
		this.setTags(wt);
		// TODO Auto-generated constructor stub
	}

}
