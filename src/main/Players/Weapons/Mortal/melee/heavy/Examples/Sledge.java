package main.Players.Weapons.Mortal.melee.heavy.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.heavy.HeavyMeleeWeapon;

public class Sledge extends HeavyMeleeWeapon{
	public Sledge(){
		super();
		this.setName("Sledge");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.BASHING);
		wt.add(WeaponTags.MELEE);
		wt.add(WeaponTags.REACHING);
		wt.add(WeaponTags.SMASHING);
		wt.add(WeaponTags.TWOHANDED);
		this.setTags(wt);
	}
}
