package main.Players.Weapons.Mortal.melee.light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.light.LightMeleeWeapon;

public class Cestus extends LightMeleeWeapon{
	public Cestus(){
		super();
		this.setName("Cestus");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.BASHING);
		wt.add(WeaponTags.BRAWL);
		wt.add(WeaponTags.SMASHING);
		wt.add(WeaponTags.WORN);
		this.setTags(wt);
	}
}
