package Dice;

import java.util.ArrayList;

public class DiceTricks {
	int autoSuccesses;
	int baseDice;
	int doubles = 10;
	ArrayList<Integer> rerollOnce;
	ArrayList<Integer> cascading;

	public DiceTricks(int autoSuccesses, int baseDice, int doubles, ArrayList<Integer> rerollOnce,
			ArrayList<Integer> cascading) {
		this.autoSuccesses = autoSuccesses;
		this.baseDice = baseDice;
		if(doubles==0) this.doubles = 10;
		else this.doubles = doubles;
		this.rerollOnce = rerollOnce;
		this.cascading = cascading;
	}
}
