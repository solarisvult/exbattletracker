package main.Players.Weapons.Mortal.melee.medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.medium.MediumMeleeWeapon;

public class SlashingSword extends MediumMeleeWeapon{
	public SlashingSword(){
		super();
		this.setName("Slashing Sword");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.MELEE);
		wt.add(WeaponTags.BALANCED);
		this.setTags(wt);
	}
}
