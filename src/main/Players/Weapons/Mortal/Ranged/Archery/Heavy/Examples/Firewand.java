package main.Players.Weapons.Mortal.Ranged.Archery.Heavy.Examples;

import java.util.ArrayList;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Archery.Heavy.HeavyArcheryWeapon;

public class Firewand extends HeavyArcheryWeapon{
	public Firewand(){
		super();
		this.setName("Firewand");
		this.putMaxRange(WeaponTags.ARCHERY, RangeBands.SHORT);
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.ARCHERY);
		wt.add(WeaponTags.FLAME);
		wt.add(WeaponTags.SLOW);
		this.setTags(wt);
	}
}
