package main.Players.Weapons.Mortal.melee.light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.light.LightMeleeWeapon;

public class Knife extends LightMeleeWeapon{
	public Knife(){
		super();
		this.setName("Knife");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.MELEE);
		wt.add(WeaponTags.THROWN);
		this.putMaxRange(WeaponTags.THROWN, RangeBands.SHORT);
		this.setTags(wt);
	}
}
