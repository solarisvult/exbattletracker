package main.Players.Weapons.Mortal.melee.medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.medium.MediumMeleeWeapon;

public class Spear extends MediumMeleeWeapon{
	public Spear(){
		super();
		this.setName("Spear");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.MELEE);
		wt.add(WeaponTags.PIERCING);
		wt.add(WeaponTags.REACHING);
		this.setTags(wt);
	}
}
