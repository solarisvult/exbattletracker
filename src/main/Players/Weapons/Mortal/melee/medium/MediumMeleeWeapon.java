package main.Players.Weapons.Mortal.melee.medium;

import java.util.TreeMap;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.Weapon;
import main.Players.Weapons.WeaponTags;

public class MediumMeleeWeapon extends Weapon{

	public MediumMeleeWeapon() {
		super(9, 1, 1);
		this.putMaxRange(WeaponTags.MELEE, RangeBands.CLOSE);
		// TODO Auto-generated constructor stub
		TreeMap<RangeBands, Integer> acc = new TreeMap<RangeBands, Integer>();
		acc.put(RangeBands.CLOSE, 2);
		acc.put(RangeBands.SHORT, 0);
		acc.put(RangeBands.MEDIUM, 0);
		acc.put(RangeBands.LONG, 0);
		acc.put(RangeBands.EXTREME, 0);
		this.setAccuracy(acc);
	}
}
