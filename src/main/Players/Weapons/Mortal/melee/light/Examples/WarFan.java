package main.Players.Weapons.Mortal.melee.light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.light.LightMeleeWeapon;

public class WarFan extends LightMeleeWeapon{
	public WarFan(){
		super();
		this.setName("War Fan");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.MARTIALARTS);
		wt.add(WeaponTags.DISARMING);
		this.setTags(wt);
	}
}
