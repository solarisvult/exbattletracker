package main.Players.Weapons.Mortal.Ranged.Archery.Medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Archery.Medium.MediumArcheryWeapon;

public class LongBow extends MediumArcheryWeapon{
	public LongBow(){
		super();
		this.setName("Long Bow");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.ARCHERY);
		this.setTags(wt);
	}
}
