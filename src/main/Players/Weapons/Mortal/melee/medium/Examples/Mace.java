package main.Players.Weapons.Mortal.melee.medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.medium.MediumMeleeWeapon;

public class Mace extends MediumMeleeWeapon{
	public Mace(){
		super();
		this.setName("Mace");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.BASHING);
		wt.add(WeaponTags.MELEE);
		wt.add(WeaponTags.SMASHING);
		this.setTags(wt);
	}
}
