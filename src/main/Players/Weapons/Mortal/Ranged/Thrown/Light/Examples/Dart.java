package main.Players.Weapons.Mortal.Ranged.Thrown.Light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Thrown.Light.LightThrownWeapon;

public class Dart extends LightThrownWeapon{
	public Dart(){
		super();
		this.setName("Dart");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.THROWN);
		wt.add(WeaponTags.CONCEALABLE);
		wt.add(WeaponTags.MOUNTED);
		//TODO IMPLEMENT POISON SELECTION BASED ON INVENTORY
		wt.add(WeaponTags.POISONABLE);
		this.setTags(wt);
	}
}
