package main;

public enum DamageModifyers{
	ZERO, ONE, TWO, FOUR, INCAPACITATED, DYING, DEAD;
	
	public int ValueToMod(){
		if(this==DamageModifyers.ZERO) return 0;
		if(this==DamageModifyers.ONE) return -1;
		if(this==DamageModifyers.TWO) return -2;
		if(this==DamageModifyers.FOUR) return -4;
		else return -10000;
	}
}