package main.Players.Armors;

public class Armor {
	private String name;
	private int soak;
	private int mobilityPenalty;
	private int hardness;
	
	public Armor(   String name,
					int soak,
					int mobilityPenalty,
					int hardness){
		this.name = name;
		this.soak = soak;
		this.mobilityPenalty = mobilityPenalty;
		this.hardness = hardness;
	}
	
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	public int getSoak() {
		return soak;
	}
	public void setSoak(int soak) {
		this.soak = soak;
	}
	public int getMobilityPenalty() {
		return mobilityPenalty;
	}
	public void setMobilityPenalty(int mobilityPenalty) {
		this.mobilityPenalty = mobilityPenalty;
	}
	public int getHardness() {
		return hardness;
	}
	public void setHardness(int hardness) {
		this.hardness = hardness;
	}	
}
