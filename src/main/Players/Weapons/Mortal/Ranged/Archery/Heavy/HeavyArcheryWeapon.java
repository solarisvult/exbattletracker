package main.Players.Weapons.Mortal.Ranged.Archery.Heavy;

import main.Players.Weapons.Mortal.Ranged.Archery.ArcheryWeapon;

public class HeavyArcheryWeapon extends ArcheryWeapon{

	public HeavyArcheryWeapon() {
		super(11, 0, 1);
	}

}
