package main.Players.Weapons.Mortal.Ranged.Thrown.Light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Thrown.Light.LightThrownWeapon;

public class Sling extends LightThrownWeapon{
	public Sling(){
		super();
		this.setName("Sling");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.BASHING);
		wt.add(WeaponTags.THROWN);
		wt.add(WeaponTags.CONCEALABLE);
		//TODO IMPLEMENT AIM ACTIONS
		this.setTags(wt);
	}
}
