package main.Players.Weapons.Mortal.melee.medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.medium.MediumMeleeWeapon;

public class SevenSectionStaff extends MediumMeleeWeapon{
	public SevenSectionStaff(){
		super();
		this.setName("Seven-Section Staff");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.BASHING);
		wt.add(WeaponTags.MARTIALARTS);
		this.putMaxRange(WeaponTags.MARTIALARTS, RangeBands.CLOSE);
		wt.add(WeaponTags.DISARMING);
		wt.add(WeaponTags.FLEXIBLE);
		this.setTags(wt);
	}
}
