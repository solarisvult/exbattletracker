package main.Players.Weapons.Mortal.Ranged.Archery.Medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Archery.Medium.MediumArcheryWeapon;

public class Crossbow extends MediumArcheryWeapon{
	public Crossbow(){
		super();
		this.setName("Crossbow");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.ARCHERY);
		wt.add(WeaponTags.CROSSBOW);
		wt.add(WeaponTags.PIERCING);
		wt.add(WeaponTags.SLOW);
		wt.add(WeaponTags.POWERFUL);
		this.setTags(wt);
	}
}
