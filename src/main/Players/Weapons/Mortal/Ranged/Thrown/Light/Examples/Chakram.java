package main.Players.Weapons.Mortal.Ranged.Thrown.Light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Thrown.Light.LightThrownWeapon;

public class Chakram extends LightThrownWeapon{
	public Chakram(){
		super();
		this.setName("Chakram");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.THROWN);
		wt.add(WeaponTags.CUTTING);
		wt.add(WeaponTags.MOUNTED);
		this.setTags(wt);
	}
}
