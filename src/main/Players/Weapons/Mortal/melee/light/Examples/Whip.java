package main.Players.Weapons.Mortal.melee.light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.light.LightMeleeWeapon;

public class Whip extends LightMeleeWeapon{
	public Whip(){
		super();
		this.setName("Whip");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.BASHING);
		wt.add(WeaponTags.MELEE);
		wt.add(WeaponTags.DISARMING);
		wt.add(WeaponTags.FLEXIBLE);
		this.setTags(wt);
	}
}