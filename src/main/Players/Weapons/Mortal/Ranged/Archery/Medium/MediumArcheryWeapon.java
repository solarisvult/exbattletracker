package main.Players.Weapons.Mortal.Ranged.Archery.Medium;

import main.Players.Weapons.Mortal.Ranged.Archery.ArcheryWeapon;

public class MediumArcheryWeapon extends ArcheryWeapon{

	public MediumArcheryWeapon() {
		super(9, 0, 1);
	}

}
