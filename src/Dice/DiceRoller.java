package Dice;

import java.util.ArrayList;
import java.util.Random;

public class DiceRoller {
	
	public DiceRoller(){
		
	}
	
	//Rolls Dice, includes dice tricks
	public DiceResults roll(DiceTricks dice){
		int successes = 0;
		Random r = new Random();
		ArrayList<Integer> rolls = new ArrayList<>();
		
		//rolls dice, applying dice tricks as defined
		for(int i = 0; i <dice.baseDice; i++){
			int roll = r.nextInt(10)+1;
			while(dice.cascading.contains(roll)){
				rolls.add(roll);
				roll = r.nextInt(10)+1;
			}
			//TODO wait on ruben's response to whether reroll once should be included in count.
			if(dice.rerollOnce.contains(roll)){
				rolls.add(roll);
				roll = r.nextInt(10)+1;
			}
			while(dice.cascading.contains(roll)){
				rolls.add(roll);
				roll = r.nextInt(10)+1;
			}
			rolls.add(roll);
		}
		//calculates successes, includes double successes
		for(int roll : rolls){
			successes+=rollToValue(roll, dice.doubles);
		}
		rolls.sort(null);
		successes+=dice.autoSuccesses;
		DiceResults results = new DiceResults(successes, rolls);
		return results;
	}
	
	//Rolls number of dice, simple roll no diceTricks
	public int roll(int numberOfDice){
		int successes = 0;
		Random r = new Random();
		ArrayList<Object> rolls = new ArrayList<>();
		for(int i = 0; i<numberOfDice; i++){
			int roll = r.nextInt(10)+1;
			int value = rollToValue(roll);
			rolls.add(roll);
			successes+=value;
		}
		//represents botched roll.
		if(successes == 0 && rolls.contains(1)) return -1;
		else return successes;
	}
	
	//default translation of dice roll to successes
	private int rollToValue(int result){
		if(result==10) return 2;
		else if(result >=7) return 1;
		else return 0;
		
	}
	//changes the minimum doubles value
	private int rollToValue(int result, int doubles){
		if(result>=doubles) return 2;
		else if (result>=7) return 1;
		else return 0;
	}
}
