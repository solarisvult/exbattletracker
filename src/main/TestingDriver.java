package main;

import main.Players.Abilities;
import main.Players.Attributes;
import main.Players.Health;
import main.Players.Players;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import Dice.*;

public class TestingDriver {

	public static void main(String[] args) {
		DiceRoller dr = new DiceRoller();
		ArrayList<Integer> r1 = new ArrayList<Integer>();
		ArrayList<Integer> r2 = new ArrayList<Integer>();
		DiceTricks dice = new DiceTricks(10,5,0, r1, r2);
		DiceResults results = dr.roll(dice);
		System.out.println(ArrayToString(results.getRolls()) + "------"+results.getRolls().size());
		System.out.println("" + results.getSuccesses());
		System.out.println("" + results.isBotched());
	}
	public static String ArrayToString(ArrayList<Integer> list){
		String listString = "";
		for (int s : list) listString = listString + s + ", ";
		return listString;
	}
}
