package main.Players;

import main.Damage;
import main.DamageModifyers;

public class Health {
	DamageModifyers modifyer;												//I hope we need this later

	public Damage[][] healthLevels;
	private final DamageModifyers values[] = DamageModifyers.values();
	
	public Health(){
		healthLevels = new Damage[6][];
		InstantiateHealth(healthLevels, 1, 1, 1, 1, 1);
		modifyer=DamageModifyers.ZERO;
	}
	
	public Health(int healthArray[]){													//instantiate, using the array as a helper value for testing
		healthLevels = new Damage[6][];
		InstantiateHealth(this.healthLevels, healthArray);
		modifyer=DamageModifyers.ZERO;

	}
	
	public int DyingHealthCount(){
		int i;
		for(i=0; i<healthLevels[healthLevels.length-1].length && healthLevels[healthLevels.length-1][i]!=Damage.UNDAMAGED;i++);
		return i;
	}
	
	public void HealHealth(int healing){
		for(int i = healthLevels.length-1; i>=0 && healing>0; i--){
			for(int j = healthLevels[i].length-1; j>=0 && healing>0; j--){
				if(healthLevels[i][j]!=Damage.UNDAMAGED){
					healing--;
					healthLevels[i][j]=Damage.UNDAMAGED;
				}
			}
		}
		UpdateDamageModifyer();
	}
	
	public void ResetDyingHealth(){
		if(this.modifyer != DamageModifyers.DEAD){
			for(int j = 0; j<healthLevels[5].length; j++){
				healthLevels[5][j]=Damage.UNDAMAGED;
			}
			UpdateDamageModifyer();
		}
	}
	
	public void DamageHealth(int bash, int leth, int aggr){  			//Helper Function
		DamageHealth(bash, leth, aggr, 0, 0);
		UpdateDamageModifyer();
	}
	
	private void DamageHealth(int bash, int leth, int aggr, int i, int j){
		//base cases begin
		if((bash == 0) && (leth == 0) && (aggr == 0)) return;						//if no more damage	
		
		else if(i==healthLevels.length-1 && bash>0 && leth==0 && aggr==0){ //if bashing overflow
			int dhl = BashingOverflow(bash, 0, 0);							//change bashing into lethal, see left over bashing
			for(int k = 0; k<healthLevels[i].length && dhl>0; k++){		
				if(healthLevels[i][k]!=Damage.LETHAL){							
					dhl--;									//changes leftovers into lethal.
					healthLevels[i][k]=Damage.LETHAL;								
				}
			}
		}
		
		else if(i>=healthLevels.length) return;									//if end of all health levels
		else if(j>=healthLevels[i].length) DamageHealth(bash, leth, aggr, i+1, 0);	//if end of one health level, new row
		
		//base cases end
		
		else if(healthLevels[i][j]==Damage.AGGRAVATED){							//if current damage is aggravated
			DamageHealth(bash, leth, aggr, i, j+1);								//try on next available spot
		}
		else if(healthLevels[i][j]==Damage.LETHAL){								//if current is Lethal
			if(aggr>0){																//if aggravated is applicable
				healthLevels[i][j]=Damage.AGGRAVATED;
				DamageHealth(bash, leth+1, aggr-1, i, j+1);
			}
			else DamageHealth(bash, leth, aggr, i, j+1);							//continue to next place
		}
		else if(healthLevels[i][j]==Damage.BASHING){							//if current is bashing
			if(aggr>0){																//if aggravated is applicable
				healthLevels[i][j]=Damage.AGGRAVATED;
				DamageHealth(bash+1, leth, aggr-1, i, j+1);
			}
			else if (leth>0){
				healthLevels[i][j]=Damage.LETHAL;									//if lethal is applicable
				DamageHealth(bash+1, leth-1, aggr-1, i, j+1);
			}
			else DamageHealth(bash, leth, aggr, i, j+1);							//continue to next place
		}
		else if(healthLevels[i][j]==Damage.UNDAMAGED){							//if current is undamaged
			if(aggr>0){																//if aggravated is applicable
				healthLevels[i][j]=Damage.AGGRAVATED;
				DamageHealth(bash, leth, aggr-1, i, j+1);
			}
			else if (leth>0){			
				healthLevels[i][j]=Damage.LETHAL;									//if lethal is applicable
				DamageHealth(bash, leth-1, aggr, i, j+1);
			}
			else if (bash>0){
				healthLevels[i][j]=Damage.BASHING;									//if bashing is applicable
				DamageHealth(bash-1, leth, aggr, i, j+1);
			}
			else return;														//no damage left to do
		}
	}
	
	private int BashingOverflow(int bash, int i, int j) {
		//base cases begin
		if(i>=healthLevels.length) return bash;			
		else if(bash==0) return bash;
		else if(j>=healthLevels[i].length) return BashingOverflow(bash, i+1, 0);
		//base cases end
		
		//begin recursion
		else if(healthLevels[i][j]==Damage.AGGRAVATED || healthLevels[i][j]==Damage.LETHAL) return BashingOverflow(bash, i, j+1);
		else if(healthLevels[i][j]==Damage.BASHING){
			healthLevels[i][j]=Damage.LETHAL;
			return BashingOverflow(bash-1, i, j+1);
		}
		
		//should be met by all the cases, but just in case
		return bash;
		
		
	}
	public void PrintHealthLevels(){
		for(int i = 0; i < healthLevels.length; i++){
			System.out.println(""+ values[i]);
			for(int j = 0; j<healthLevels[i].length; j++){
				System.out.println("" + healthLevels[i][j]);
			}
		}
		
	}
	
	public void PrintDamageModifyer(){
		System.out.println("" + this.modifyer);
	}
	
	public void UpdateDamageModifyer(){											//updates modifyer based on damage taken
		int i;
		for(i = 0; i<this.healthLevels.length && this.healthLevels[i][0]!=Damage.UNDAMAGED; i++);
		if(i==this.healthLevels.length){
			if(this.healthLevels[i-1][this.healthLevels[i-1].length-1] == Damage.UNDAMAGED)
				this.modifyer= this.values[i-1];
			else this.modifyer = this.values[i];
		}
		else if(i==0) this.modifyer= this.values[0]; 
		else this.modifyer = this.values[i-1];
	}
	
	public void UpdateHealth(int healthArray[]){
		Damage newHealth[][] = new Damage[6][];
		InstantiateHealth(newHealth, healthArray);
		this.healthLevels=newHealth;
		
	}
	
	private void InstantiateHealth(Damage[][] healthLevels, int zero, int one, int two, int four, int dhl){
		zero=Math.abs(zero);
		one=Math.abs(one);
		two=Math.abs(two);
		four=Math.abs(four);
		dhl=Math.abs(dhl);
		healthLevels[0]=new Damage[zero];
		healthLevels[1]=new Damage[one];
		healthLevels[2]=new Damage[two];
		healthLevels[3]=new Damage[four];
		healthLevels[4]=new Damage[1];
		healthLevels[5]=new Damage[dhl];
		InstantiateHealth(this.healthLevels);
	}
	

	private void InstantiateHealth(Damage healthLevels[][], int healthArray[]){
		if(healthArray.length==5){
			for(int i = 0; i<4; i++){
				healthArray[i]=Math.abs(healthArray[i]);
				healthLevels[i] = new Damage[healthArray[i]];
			}
				healthLevels[4] = new Damage[1];
				healthLevels[5] = new Damage[healthArray[4]];
			InstantiateHealth(this.healthLevels);
		}
		else System.out.println("ERROR: Array not of length 6");	//invalid array
	}
	
	private void InstantiateHealth(Damage[][] health) {
		for(int i =0; i<health.length; i++){
			for(int j = 0; j<health[i].length; j++) health[i][j]=Damage.UNDAMAGED;
		}
	}
}
