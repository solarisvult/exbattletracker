package main.Players.Weapons.Mortal.melee.medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.medium.MediumMeleeWeapon;

public class Staff extends MediumMeleeWeapon{
	public Staff(){
		super();
		this.setName("Staff");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.BASHING);
		wt.add(WeaponTags.MELEE);
		wt.add(WeaponTags.REACHING);
		this.setTags(wt);
	}
}
