//this is a sample character sheet

//Attributes will have Attribute number. This is a sample, so it will appear like so.

Attributes
STR 3 
DEX 4
STA 1
CHA 5
MAN 5
APP 5
PER 1
INT 2
WIT 3

//abilities will also have the same system, except we should say which is the current caste as well
Abilities
//This character is a Dawn, so all of the dawns will be favored.
//F = Favored
//U = UnFavored
Dawn F
Archery 5 F
MartialArts 5 F
Melee 5 F
Thrown 5 F
War 5 F

Night U
Athletics 5 F
Awareness 5 F
Dodge 1 U
Larceny 1 U
Stealth 1 U

Zenith
Integrity 3 F
Performance 1 U
Presence 0 U
Resistance 0 U
Survivaly 0 U

Twilight U
								
Craft							//Idea 1 Crafting 
Earth 3 F						//I Think Craft should begin it's own block that ends with
Wood 2 U						//With this example, we would not include any craft that doesn't 
								//have any points with it 
END								// then there would be a ending symbol for the end of the section

Craft							//Idea 2 Crafting
Earth 3 F						//just have all crafts and continue like usual.
Wood 2 U						//if twilight, then all of these will be favored.
Water 0 U
Fire 0 U
Air 0 U
Magitech 0 U
								//Ending Symbol Is optional under this format. 
								//ultimately not necessary								
Investigation 0 U
Lore 0 U
Medicine 0 U
Occult 0 U

Eclipse
Bureaucracy 0 U
Linguistics 0 U
Ride 0 U
Sail 0 U
Socialize 0 U

Specialities
Melee 3 F Greatsword 	//Speciality formats, The system would be need to run through the specialties.
						//We'd need to have a tag on the weapons to say if they work with the specialty?
Archery 3 F Crossbows
Athletics 2 F Falling

Weapons
GreatSword Speed 5 Accuracy 3 2 Damage 3B 2L Defense 1 0 Range 100 Rate 2 
Attribute STR Ability Melee Speciality Greatsword 
					//we might not need to track range, the GM could use the mental strength to decide
					//whether an attack is viable. However, if we were to expand we would need to keep it
					//Each Damage/Accuracy is helpful. I do not think it's necessary to say 3B if bashing is
					//always first. But who knows.
					//tracking what stat to use may be useful.
					
					//I think we should have a Weapon Class that holds all the variables, and then have the 
					//character class hold each weapon. Then whatever weapon is first would be the 
					//equiped weapon.
					//we'd do the same thing with armor. 
Armor
Lamalar Type Light Soak 6B 3L Fatigue -1 Mobility -2

Health
Soak 6B 3L 3A
Hardness 8N 2L 2A
Willpower 5

Zero 4 0		//each health is filled by the max at that level, plus how many is currently used.
One 2 0
One2 0 0
Two 4 0
Four 3 0
Incapacitated 1 0	//we could cut this down to 1 number, since you only have one level of this.
DHL 5 0

DV
Parry 6
Dodge 5

Essence
Personal 14 0      //same as health, left is Max, right is current in use.
Peripheral 34 0    
Extra  6 0         //In case there is a battery somewhere


Limit 5 	//this portion isn't necessary for battle, but might be beneficial in the future.