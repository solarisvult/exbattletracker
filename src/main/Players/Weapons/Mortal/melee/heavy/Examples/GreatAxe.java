package main.Players.Weapons.Mortal.melee.heavy.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.heavy.HeavyMeleeWeapon;

public class GreatAxe extends HeavyMeleeWeapon{
	public GreatAxe(){
		super();
		this.setName("Great Axe");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.MELEE);
		wt.add(WeaponTags.CHOPPING);
		wt.add(WeaponTags.REACHING);
		wt.add(WeaponTags.TWOHANDED);
		this.setTags(wt);
	}
}
