package main.Players.Weapons.Mortal.melee.light.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.light.LightMeleeWeapon;

public class Club extends LightMeleeWeapon{
	public Club(){
		super();
		this.setName("Club");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.BASHING);
		wt.add(WeaponTags.MELEE);
		wt.add(WeaponTags.SMASHING);
		this.setTags(wt);
	}
}
