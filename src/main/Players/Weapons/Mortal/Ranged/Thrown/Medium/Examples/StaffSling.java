package main.Players.Weapons.Mortal.Ranged.Thrown.Medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Thrown.Medium.MediumThrownWeapon;

public class StaffSling extends MediumThrownWeapon{
	public StaffSling(){
		super();
		this.setName("Staff Sling");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.BASHING);
		wt.add(WeaponTags.THROWN);
		//IMPLEMENT AIM ACTION
		this.setTags(wt);
	}
}
