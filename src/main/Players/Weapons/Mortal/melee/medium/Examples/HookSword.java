package main.Players.Weapons.Mortal.melee.medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.medium.MediumMeleeWeapon;

public class HookSword extends MediumMeleeWeapon{
	public HookSword(){
		super();
		this.setName("Hook Sword");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.MARTIALARTS);
		wt.add(WeaponTags.DISARMING);
		this.setTags(wt);
	}
}
