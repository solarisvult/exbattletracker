package main.Players.Weapons.Mortal.Ranged.Thrown.Heavy;

import java.util.TreeMap;

import main.Players.Weapons.RangeBands;
import main.Players.Weapons.Weapon;
import main.Players.Weapons.WeaponTags;

public class HeavyThrownWeapon extends Weapon{

	public HeavyThrownWeapon() {
		super(11, 0, 1);
		this.putMaxRange(WeaponTags.THROWN, RangeBands.MEDIUM);
		TreeMap<RangeBands, Integer> RB = new TreeMap<RangeBands, Integer>();
		RB.put(RangeBands.CLOSE, 4);
		RB.put(RangeBands.SHORT, 3);
		RB.put(RangeBands.MEDIUM, 2);
		RB.put(RangeBands.LONG, -1);
		//TODO MAKE SO THROWN CANNOT GO TO EXTREME RANGE WITHOUT CHARMS
		RB.put(RangeBands.EXTREME, -3);
		this.setAccuracy(RB);
	}

}
