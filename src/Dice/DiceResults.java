package Dice;

import java.util.ArrayList;

public class DiceResults {
	int successes;
	ArrayList<Integer> rolls;
	boolean botched = false;
	
	public DiceResults(int successes, ArrayList<Integer> rolls) {
		this.successes = successes;
		this.rolls = rolls;
		if(successes == 0 && rolls.contains(1)) this.botched = true;
	}

	public int getSuccesses() {
		return successes;
	}

	public void setSuccesses(int successes) {
		this.successes = successes;
	}

	public ArrayList<Integer> getRolls() {
		return rolls;
	}

	public void setRolls(ArrayList<Integer> rolls) {
		this.rolls = rolls;
	}

	public boolean isBotched() {
		return botched;
	}

	public void setBotched(boolean botched) {
		this.botched = botched;
	}
	
	
	
}
