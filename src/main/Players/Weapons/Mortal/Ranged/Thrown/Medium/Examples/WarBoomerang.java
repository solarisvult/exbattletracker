package main.Players.Weapons.Mortal.Ranged.Thrown.Medium.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.Ranged.Thrown.Medium.MediumThrownWeapon;

public class WarBoomerang extends MediumThrownWeapon{
	public WarBoomerang(){
		super();
		this.setName("War Boomerang");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.THROWN);
		wt.add(WeaponTags.CUTTING);
		wt.add(WeaponTags.MOUNTED);
		this.setTags(wt);
	}
}
