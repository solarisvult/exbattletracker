package main.Players;
import java.util.Scanner;
import java.util.TreeMap;
import main.Players.Abilities;
import main.Players.Armors.Armor;
import main.Players.Weapons.Weapon;

public class Players {
	private TreeMap<Attributes, Integer> AttributesMap;			//Player Stats are held in both teams
	private TreeMap<Abilities, Integer> AbilitiesMap;
	private Weapon primaryWeapon;
	private TreeMap<String, Weapon> WeaponSet;
	private Armor primaryArmor;
	private TreeMap<String, Armor> ArmorSet;
	private int initiative;
	public Health HealthLevels;
	
	public Players(){											//Player initalization
		AttributesMap = new TreeMap<Attributes, Integer>();
		AbilitiesMap = new TreeMap<Abilities, Integer>();
		WeaponSet = new TreeMap<String, Weapon>();
		ArmorSet = new TreeMap<String, Armor>();
		InitializeAttributes();
		InitializeAbilities();
		initiative = 0;
		HealthLevels = new Health();
	}
	//TODO AUTOMATED TO ATTACK USING DICE ROLLER
	//TODO HAVE AN ATTACK THAT OUTPUTS DAMAGE PACKAGE FOR DEFENDERS
	//TODO IMPLEMENT DEFENSE CLASS
	//TODO IMPLEMENT MISC INVENTORY
	public int DecisiveAttack(){
		//TODO GET ABILITY ON WEAPON
		int attackRoll = AttributesMap.get(Attributes.DEXTERITY);
					   //TODO GET ABILITY FROM WEAPON
		System.out.println("Your base roll is: " + attackRoll);
		System.out.println("Include charms, specialities, and stunt dice");
		Scanner reader = new Scanner(System.in);
		System.out.println("What is Sucesses?");
		int successes = reader.nextInt();
		System.out.println("What is the opponenets Defense");
		int defense = reader.nextInt();
		if(successes<defense){
			if(initiative<10)UpdateInitiative(-2);
			else UpdateInitiative(-3);
			return 0;
		}
		System.out.println("Roll your initiative: " + this.initiative);
		System.out.println("How many Successes? No Double 10s");
		int damage = reader.nextInt();
		System.out.println("What is your opponnent's hardness?");
		int hardness = reader.nextInt();
		if(damage<hardness){
			this.setIntiative(3);
			return 0;
		}
		else{
			this.setIntiative(3);
			return damage;
		}
	}
	
	public int WitheringAttack(){
		//TODO MAKE pick ability SWITCH STATEMENT BASED ON TAG
		//TODO MAKE GetACcuracy based on RangeBands
		//int attackRoll = primaryWeapon.getAccuracy()
	//					+ AttributesMap.get(Attributes.DEXTERITY)
						//TODO CHANGE GET COMBAT TO SOMETING ELSE
		//				- HealthLevels.modifyer.ValueToMod();
		
		//System.out.println("Base Roll is " + attackRoll);
		System.out.println("Include Specialties and charm dice");
		System.out.println("What was your roll?");
		Scanner reader = new Scanner(System.in);
		int toHit = reader.nextInt();
		System.out.println("What is the opponents defense?");
		toHit-= reader.nextInt();
		if(toHit<0) {
			reader.close();
			return 0;
		}
		System.out.println("What is your opponenets Soak?");
		int soak = reader.nextInt();
		//TODO change this to not include player strength on certain weapons, (i.e. Crossbows, Firewands)
		int rawDamage = primaryWeapon.getDamage()
						+ toHit
						//TODO CHANGE TO OPTIONAL INCLUDE STRENGTH
						- soak;
		if(rawDamage<primaryWeapon.getOverwhelming()) System.out.println("Roll " 
																		+ primaryWeapon.getOverwhelming()
																		+ " Dice");
		else System.out.println("Roll "
							   	+ rawDamage
							   	+ " Dice");
		System.out.println("How many successes?");
		int initiativeShift = reader.nextInt();
		UpdateInitiative(initiativeShift);
		
		reader.close();
		return initiativeShift;
	}
	public void JoinBattle(){
		int diceRolled = 0;
		diceRolled+=AttributesMap.get(Attributes.WITS) + AbilitiesMap.get(Abilities.AWARENESS);
		System.out.println("Join Battle! Roll " 
				+ (diceRolled-HealthLevels.modifyer.ValueToMod())
				+ " dice!");
		Scanner reader = new Scanner(System.in);
		System.out.println("How Many Successes?");
		this.initiative = reader.nextInt() + 3;
		reader.close();
	}
	
	public int getInitiative(){
		return this.initiative;
	}
	public void setIntiative(int initiative){
		this.initiative = initiative;
	}
	public void UpdateInitiative(int value){
		initiative+=value;
	}
	public void SwapPrimaryArmor(String swappedArmor){
		if(ArmorSet.containsKey(swappedArmor))primaryArmor = ArmorSet.get(swappedArmor);
	}
	public void SwapPrimaryArmor(Armor swappedArmor){
		if(ArmorSet.containsValue(swappedArmor)) primaryArmor = swappedArmor;
	}
	public void AddArmor(Armor newArmor){
		if(primaryArmor == null) primaryArmor = newArmor;
		ArmorSet.put(newArmor.getName(), newArmor);
	}
	public void SwapPrimaryWeapon(Weapon newWeapon){
		if(WeaponSet.containsValue(newWeapon)) primaryWeapon = newWeapon;
	}
	public void SwapPrimaryWeapon(String swappedWeapon){
		if(WeaponSet.containsKey(swappedWeapon))primaryWeapon = WeaponSet.get(swappedWeapon);
	}
	public void AddWeapon(Weapon newWeapon){
		WeaponSet.put(newWeapon.getName(), newWeapon);
		if(primaryWeapon == null){
			primaryWeapon = newWeapon;
		}
	}
	
	public int GetStat(Attributes stat){						//Returns an Attribute
		return AttributesMap.get(stat).intValue();
	}
	public int GetStat(Abilities stat){							//returns any ability
		return AbilitiesMap.get(stat).intValue();
	}
	
	public void UpdateStat(Attributes stat, int value){			//changes any Attribute
		AttributesMap.put(stat, Integer.valueOf(value));
	}
	
	public void UpdateStat(Abilities stat, int value){			//Changes any Ability
		AbilitiesMap.put(stat, Integer.valueOf(value));
	}
	
	private void InitializeAttributes(){						//used during initialization to insure
		for(Attributes g : Attributes.values()){				//all attributes are in Tree
			AttributesMap.put(g, Integer.valueOf(0));
		}
	}
	
	private void InitializeAbilities(){							//used during initialization to insure
		for(Abilities g : Abilities.values()){					//all abilities are in tree.
			AbilitiesMap.put(g, Integer.valueOf(0));
		}
	}


	public TreeMap<Attributes, Integer> getAttributesMap() {
		return AttributesMap;
	}


	public void setAttributesMap(TreeMap<Attributes, Integer> attributesMap) {
		AttributesMap = attributesMap;
	}


	public TreeMap<Abilities, Integer> getAbilitiesMap() {
		return AbilitiesMap;
	}


	public void setAbilitiesMap(TreeMap<Abilities, Integer> abilitiesMap) {
		AbilitiesMap = abilitiesMap;
	}


	public Weapon getPrimaryWeapon() {
		return primaryWeapon;
	}


	public void setPrimaryWeapon(Weapon primaryWeapon) {
		this.primaryWeapon = primaryWeapon;
	}


	public TreeMap<String, Weapon> getWeaponSet() {
		return WeaponSet;
	}


	public void setWeaponSet(TreeMap<String, Weapon> weaponSet) {
		WeaponSet = weaponSet;
	}


	public Armor getPrimaryArmor() {
		return primaryArmor;
	}


	public void setPrimaryArmor(Armor primaryArmor) {
		this.primaryArmor = primaryArmor;
	}


	public TreeMap<String, Armor> getArmorSet() {
		return ArmorSet;
	}


	public void setArmorSet(TreeMap<String, Armor> armorSet) {
		ArmorSet = armorSet;
	}


	public Health getHealthLevels() {
		return HealthLevels;
	}


	public void setHealthLevels(Health healthLevels) {
		HealthLevels = healthLevels;
	}
	
	
}
