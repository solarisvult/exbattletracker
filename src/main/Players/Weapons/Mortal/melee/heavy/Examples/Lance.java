package main.Players.Weapons.Mortal.melee.heavy.Examples;

import java.util.ArrayList;

import main.Players.Weapons.WeaponTags;
import main.Players.Weapons.Mortal.melee.heavy.HeavyMeleeWeapon;

public class Lance extends HeavyMeleeWeapon{
	public Lance(){
		super();
		this.setName("Lance");
		ArrayList<WeaponTags> wt = new ArrayList<WeaponTags>();
		wt.add(WeaponTags.LETHAL);
		wt.add(WeaponTags.MELEE);
		wt.add(WeaponTags.PIERCING);
		wt.add(WeaponTags.REACHING);
		wt.add(WeaponTags.TWOHANDED);
		this.setTags(wt);
	}
}
